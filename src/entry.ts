import './scss/app.scss'
import { App } from './ts/App'

document.addEventListener('DOMContentLoaded', () => {
    let app: App = new App();
});
##Description
***

This is just a quickstart project I made to help facilitate faster upstart time and cut down on project creation. Once
you've made a typescript webpack more than a few times it loses it's fun. I hope to add to this over time to create a 
more robust starting point

The entry point is entry.ts under the source folder (src). It creates an App object on DOMContentLoaded. This file is
also where we hook in the main sass file. All source files are generated into respective css and js files and stored
in their respective folders in the public folder on compilation.

##Installation
***

  1. clone project
  2. npm install
  3. npm run watch

##Npm commands
***
  * build: builds the project
  * watch: watches for changes and re-compiles

##Other Information
***

#####Webpack

  * Typescript compilation
  * Sass compilation
  * Browser sync

#####Public Directory

  * Typescript and Sass compilation generate here in respective css / js folders. These get created on the initial watch
run
  * The index.html is your main html entry point.
  * The public folder is encapsulated from the rest of the project.